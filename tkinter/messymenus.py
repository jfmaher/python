from tkinter import *
root = Tk()

menubar = Menu(root)
menu_file = Menu(menubar, tearoff=0)
menu_file.add_command(label='Exit', command=root.quit)
menu_edit = Menu(menubar, tearoff=0)
menubar.add_cascade(menu=menu_file, label='File')
menubar.add_cascade(menu=menu_edit, label='Edit')

root['menu'] = menubar

frame = Label(root, text='right click here')
menu = Menu(frame, tearoff=0)
for i in ('One', 'Two', 'Three'):
  menu.add_command(label=i)
  if (root.tk.call('tk', 'windowingsystem')=='aqua'):
    frame.bind('<2>', lambda e: menu.post(e.x_root, e.y_root))
    frame.bind('<Control-1>', lambda e: menu.post(e.x_root, e.y_root))
  else:
    frame.bind('<3>', lambda e: menu.post(e.x_root, e.y_root))

frame.grid(column=0, row=0)

root.mainloop()
