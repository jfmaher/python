from tkinter import *

class MyButton(Button):
  def __init__(self, frame, *args, **kwargs):
    super(MyButton, self).__init__(frame, *args, **kwargs)
    self.config(font = ("Helvetica", 10, "bold"))

class App:

  def __init__(self, master):
  
    frame = Frame(master)
    frame.pack()

    self.button = MyButton(
      frame, text="QUIT", fg='red', command=frame.quit
      )
    self.button.pack(side=LEFT)

    self.hi_there = MyButton(frame, text="Hello", command=self.say_hi)
    self.hi_there.pack(side=LEFT)

    self.change = MyButton(frame, text='change colour', command=self.change)
    self.change.pack(side=LEFT)

  def change(self):
    if self.button.cget('fg') == 'red':
      self.button.configure(fg = 'yellow')
    else:
      self.button.configure(fg = 'red')

  def say_hi(self):
    print("hi there, everyone!")

root = Tk()

app = App(root)

root.mainloop()
