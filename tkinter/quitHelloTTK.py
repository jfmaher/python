from tkinter import *
from tkinter import ttk

class App:
  def __init__(self, master):
    ttk.Style().configure('TButton', font=('Helvetica', 10))
    ttk.Style().configure('yel.TButton', foreground='yellow')
    ttk.Style().configure('red.TButton', foreground='red')
  
    frame = Frame(master)
    frame.pack()

    self.button = ttk.Button(frame, style='yel.TButton', text='QUIT', command=frame.quit)
    self.button.pack(side=LEFT)

    self.hi_there = ttk.Button(frame, text="Hello", command=self.say_hi)
    self.hi_there.pack(side=LEFT)

    self.change = ttk.Button(frame, text='change colour', command=self.change)
    self.change.pack(side=LEFT)

  def change(self):
    if self.button.cget('style') == 'red.TButton':
      self.button.configure(style = 'yel.TButton')
    else:
      self.button.configure(style = 'red.TButton')

  def say_hi(self):
    print("hi there, everyone!")

root = Tk()

app = App(root)

root.mainloop()
