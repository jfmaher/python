from tkinter import *
from tkinter import ttk

root = Tk()
root.title('Scaler')

def onValidate(action, index, value_if_allowed,
             prior_value, text, validation_type, trigger_type, widget_name):
  if text in '0123456789':
    try:
      int(value_if_allowed)
      return True
    except ValueError:
      return False
  else:
    return False

vcmd = (root.register(onValidate),
        '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')

num = StringVar()
num.set(str(0))
numbox = ttk.Entry(root, textvariable = num, validate='key', validatecommand=vcmd)

answertxt = StringVar()
answertxt.set(str(0))
def update(this):
  answertxt.set(int(num.get()) * scale.get())

mulsign = ttk.Label(root, text='*')

scale = ttk.Scale(root, orient=VERTICAL, from_=0, to=20, command=update)

eqsign = ttk.Label(root, text='=')

answer = ttk.Label(root, textvariable = answertxt, width=-10, borderwidth=10)

numbox.grid(column=0, row=0)
mulsign.grid(column=1, row=0)
scale.grid(column=2, row=0)
eqsign.grid(column=4, row=0)
answer.grid(column=5, row=0)

root.mainloop()
